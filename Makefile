.PHONY: all
all: testbits plt-stress

libtest.so: src/testlib.c src/testapi.h
	gcc -shared -o $@ $^ -fPIC
libtestaudit-eager.so: src/testaudit.c src/testapi.h
	gcc -shared -o $@ -DDO_EAGER_SCAN $^ -fPIC -ldl `pwd`/libtest.so
libtestaudit-lazy.so: src/testaudit.c src/testapi.h
	gcc -shared -o $@ -DDO_LAZY_SCAN $^ -fPIC -ldl `pwd`/libtest.so
libtestaudit-bipolar.so: src/testaudit.c src/testapi.h
	gcc -shared -o $@ -DDO_LAZY_SCAN -DDO_EAGER_SCAN $^ -fPIC -ldl `pwd`/libtest.so
libtestaudit-snail.so: src/testaudit.c src/testapi.h
	gcc -shared -o $@ -DDO_SNAIL_MODE $^ -fPIC -ldl `pwd`/libtest.so
libtestaudit-stable.so: src/testaudit.c src/testapi.h
	gcc -shared -o $@ $^ -fPIC -ldl `pwd`/libtest.so
.PHONY: testbits
testbits: testrun-eager.sh testrun-lazy.sh testrun-stable.sh libtest.so
testbits: libtestaudit-eager.so libtestaudit-lazy.so libtestaudit-bipolar.so libtestaudit-stable.so libtestaudit-snail.so

src/libplt-stress.so: src/plt-stress-inner.c
	gcc -shared -o $@ $^ -fPIC
plt-stress: src/plt-stress.c src/libplt-stress.so
	gcc -O0 -o $@ $^ -Wl,--rpath `pwd`
