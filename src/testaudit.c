#define _GNU_SOURCE
#include "testapi.h"

#include <stdlib.h>
#include <link.h>
#include <dlfcn.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <stdbool.h>
#include <time.h>

static bool appinit = false;
static bool appinitdone = false;
static bool connected = false;
static bool trackerstable = false;
static bool disconnected = false;
static PFN_opened opened = NULL;
static PFN_closed closed = NULL;
static PFN_found found = NULL;
static PFN_bound bound = NULL;
static PFN_init init = NULL;
static char* tracker = NULL;

static void try_connect(struct link_map* map);
static void scan(struct link_map* map, uintptr_t* cookie);

unsigned int la_version(unsigned int ver) {
  printf("[audit] Loading auditor, API version %u\n", ver);
  tracker = getenv("TESTRUN_LIBRARY");
  if(tracker == NULL) {
    printf("[audit] TESTRUN_LIBRARY not set in the environment, disabling auditing.\n");
    return 0;
  }
  printf("[audit] Awaiting tracker `%s'\n", tracker);
  fflush(stdout);
  tracker = strdup(tracker);  // In case someone uses setenv
  return ver;
}

static int callback(struct dl_phdr_info *info, size_t size, void *data) {
  printf("[audit] dl_iterate_phdr reports `%s' at %zx\n", info->dlpi_name, info->dlpi_addr);
  return 0;
}

void la_activity(uintptr_t* cookie, unsigned int flag) {
  if(flag == LA_ACT_ADD)
    printf("[audit] Link map unstable: adding entries\n");
  else if(flag == LA_ACT_DELETE)
    printf("[audit] Link map unstable: removing entries\n");
  else if(flag == LA_ACT_CONSISTENT)
    printf("[audit] Link map stablized\n");
  fflush(stdout);

  if(flag == LA_ACT_ADD) appinit = true;

  if(!trackerstable && connected && !disconnected) {
    if(flag == LA_ACT_CONSISTENT) {
      printf("[audit] Connection to tracker stablized\n");
      dl_iterate_phdr(callback, NULL);
      fflush(stdout);
      trackerstable = true;
      init();
    }
  }
}

#if defined(DO_LAZY_SCAN) || defined(DO_SNAIL_MODE)
#define BINDFROM LA_FLG_BINDFROM
#define BINDTO LA_FLG_BINDTO
#else
#define BINDFROM 0
#define BINDTO 0
#endif

unsigned int la_objopen(struct link_map* map, Lmid_t lmid, uintptr_t* cookie) {
  if(appinit && !appinitdone) {
    appinitdone = true;
    // vdso doesn't have an objopen notification, so hunt it down via the linkmap.
    struct link_map* head;
    for(head = map; head->l_prev != NULL; head = head->l_prev);
    for(; head != map; head = head->l_next)
      printf("[audit] Found extra prior map: `%s' at %zx\n", head->l_name, head->l_addr);
  }

  // As a special case, the main application binary is always listed with an empty string.
  // So we use /proc/self/exe to find its actual path. (And then only use it for the size.)
  struct stat st;
  if(map->l_name[0] == '\0') {
    size_t sz = 4096;
    char* name = malloc(sz);
    ssize_t res;
    while((res = readlink("/proc/self/exe", name, sz)) == sz) {
      sz += 1024;
      name = realloc(name, sz);
    }
    if(res < 0) {
      int e = errno;
      char buf[1024];
      const char* estr = strerror_r(e, buf, sizeof buf);
      printf("[audit] Failed to readlink /proc/self/exe: %s\n", estr);
    } else {
      if(stat(name, &st) != 0) {
        int e = errno;
        char buf[1024];
        const char* estr = strerror_r(e, buf, sizeof buf);
        printf("[audit] Failed to stat main application `%s': %s (mapped at %zx)\n", name, estr, map->l_addr);
        *cookie = UD_MISSED;
        scan(map, cookie);
        return BINDFROM | BINDTO;
      }
      printf("[audit] Found main application `%s' (reported as `' in later messages)\n", name);
    }
    free(name);
  }

  // Get the file size from stat(). According to the man page, this will
  // work even without read permission (but it does need exec on the path).
  if(map->l_name[0] != '\0' && stat(map->l_name, &st) != 0) {
    int e = errno;
    char buf[1024];
    const char* estr = strerror_r(e, buf, sizeof buf);
    printf("[audit] Failed to stat `%s': %s (mapped at %zx)\n", map->l_name, estr, map->l_addr);
    *cookie = UD_MISSED;
    scan(map, cookie);
    return BINDFROM | BINDTO;
  }

  // Make sure we're connected before going too far.
  if(!connected) {
    try_connect(map);
    if(connected) {
      printf("[audit] Found tracker library, mapped at [%zx,%zx)\n",
        map->l_addr, map->l_addr + st.st_size);
      *cookie = UD_TRACKER;
      scan(map, cookie);
      return BINDTO;
    } else {
      printf("[audit] Open before connection: [%zx,%zx) -> `%s'\n",
        map->l_addr, map->l_addr + st.st_size, map->l_name);
      *cookie = UD_MISSED;
      scan(map, cookie);
      if(strstr(map->l_name, "/ld-") != NULL) { printf("[audit] Recognized as ld.so\n"); return BINDTO; }
      if(strstr(map->l_name, "/libdl.so") != NULL) { printf("[audit] Recognized as libdl.so\n"); return BINDTO; }
      if(strstr(map->l_name, "/libc.so") != NULL) { printf("[audit] Recognized as libc.so\n"); return BINDTO; }
      return BINDFROM | BINDTO;
    }
  }

  // If the link map hasn't stablized since connection, we need to give
  // it a moment. Otherwise nothing's relocated yet.
  if(!trackerstable) {
    printf("[audit] Open before stablization: [%zx,%zx) -> `%s'\n",
      map->l_addr, map->l_addr + st.st_size, map->l_name);
    *cookie = UD_MISSED;
    scan(map, cookie);
    if(strstr(map->l_name, "/ld-") != NULL) { printf("[audit] Recognized as ld.so\n"); return BINDTO; }
    if(strstr(map->l_name, "/libdl.so") != NULL) { printf("[audit] Recognized as libdl.so\n"); return BINDTO; }
    if(strstr(map->l_name, "/libc.so") != NULL) { printf("[audit] Recognized as libc.so\n"); return BINDTO; }
    return BINDFROM | BINDTO;
  }

  // We *might* have had our connection cut by this point. Really shouldn't
  // happen, but technically possible.
  if(disconnected) {
    printf("[audit] Open after disconnection: [%zx,%zx) -> `%s'\n",
      map->l_addr, map->l_addr + st.st_size, map->l_name);
    *cookie = UD_MISSED;
    scan(map, cookie);
    return BINDFROM | BINDTO;
  }

  // Figure out the ranges and notify the tracker.
  opened(map->l_addr, map->l_addr + st.st_size, map->l_name, cookie);
  scan(map, cookie);
  return BINDFROM | BINDTO;
}

unsigned int la_objclose(uintptr_t* cookie) {
  if(*cookie == UD_MISSED) {
    //printf("[audit] Missing close: missed open\n");
  } else if(*cookie == UD_TRACKER) {
    printf("[audit] Tracker disconnected\n");
    disconnected = true;
    free(tracker);
  } else if(!connected || disconnected) {
    printf("[audit] Skipping close after disconnection\n");
  } else {
    closed(cookie);
  }
  fflush(stdout);
  return 0;
}

#ifdef DO_LAZY_SCAN
uintptr_t la_symbind64(Elf64_Sym* sym, unsigned int idx, uintptr_t* refcook, uintptr_t* defcook,
                       unsigned int* flags, const char* name) {
  struct timespec ts = {0, 1000};
  clock_nanosleep(CLOCK_REALTIME, 0, &ts, NULL);
  if(!trackerstable) {
    printf("[audit] Symbol bind before stablization: `%s'\n", name);
    fflush(stdout);
    return sym->st_value;
  }

  bound(refcook, defcook, name, sym->st_value, sym->st_value + sym->st_size);
  return sym->st_value;
}
#endif

#ifdef DO_SNAIL_MODE
Elf64_Addr la_x86_64_gnu_pltenter(Elf64_Sym* sym, unsigned int ndx, uintptr_t* ref, uintptr_t* def, La_x86_64_regs* regs,
                                  unsigned int* flags, const char* symname, long int* framesize) {
  struct timespec ts = {0, 1000};
  clock_nanosleep(CLOCK_REALTIME, 0, &ts, NULL);
  return sym->st_value;
}
#endif

static void try_connect(struct link_map* map) {
  // Check if the path is what we expect for the tracker
  if(strcmp(map->l_name, tracker) != 0) return;

  // Nab the STRTAB and SYMTAB
  const char* strtab = NULL;
  ElfW(Sym)* symtab = NULL;
  for(const ElfW(Dyn)* d = map->l_ld; d->d_tag != DT_NULL; d++) {
    if(d->d_tag == DT_STRTAB) strtab = (const char*)d->d_un.d_ptr;
    else if(d->d_tag == DT_SYMTAB) symtab = (ElfW(Sym)*)d->d_un.d_ptr;
    if(strtab && symtab) break;
  }
  if(!strtab || !symtab) return;  // Probably a VERY bad thing, but hey.

  // Now scan for our tracker's hooks
  for(ElfW(Sym)* s = symtab; !opened || !closed || !bound || !found || !init; s++) {
    if(ELF64_ST_TYPE(s->st_info) != STT_FUNC) continue;
    if(strcmp(&strtab[s->st_name], "testrun_opened") == 0) {
      opened = (PFN_opened)(map->l_addr + s->st_value);
    } else if(strcmp(&strtab[s->st_name], "testrun_closed") == 0) {
      closed = (PFN_closed)(map->l_addr + s->st_value);
    } else if(strcmp(&strtab[s->st_name], "testrun_found") == 0) {
      found = (PFN_found)(map->l_addr + s->st_value);
    } else if(strcmp(&strtab[s->st_name], "testrun_bound") == 0) {
      bound = (PFN_bound)(map->l_addr + s->st_value);
    } else if(strcmp(&strtab[s->st_name], "testrun_init") == 0) {
      init = (PFN_init)(map->l_addr + s->st_value);
    }
  }

  // Mark that we have connected
  connected = true;
}

static void scan(struct link_map* map, uintptr_t* cookie) {
#ifdef DO_EAGER_SCAN
  // Nab the STRTAB and SYMTAB
  const char* strtab = NULL;
  size_t strsz = 0;
  ElfW(Sym)* symtab = NULL;
  for(const ElfW(Dyn)* d = map->l_ld; d->d_tag != DT_NULL; d++) {
    if(d->d_tag == DT_STRTAB) strtab = (const char*)d->d_un.d_ptr;
    else if(d->d_tag == DT_SYMTAB) symtab = (ElfW(Sym)*)d->d_un.d_ptr;
    else if(d->d_tag == DT_STRSZ) strsz = d->d_un.d_val;
    if(strtab && symtab && strsz) break;
  }
  if(!strtab || !symtab || !strsz) return;  // Probably a VERY bad thing, but hey.

  // We can't scan all the symbols, so just print the name of the first function.
  for(ElfW(Sym)* s = symtab; s->st_name < strsz; s++) {
    if(ELF64_ST_TYPE(s->st_info) != STT_FUNC) continue;
    if(s->st_shndx == SHN_UNDEF) continue;
    const size_t base = map->l_addr + s->st_value;
    if(trackerstable)
      found(cookie, &strtab[s->st_name], base, base + s->st_size);
    else
      printf("[audit] Found function before stablization: [%zx,%zx) -> `%s'\n", base, base+s->st_size, &strtab[s->st_name]);
    fflush(stdout);
  }
#endif
}
