#define _GNU_SOURCE
#include "testapi.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <link.h>

__attribute__((constructor))
static void testrun_my_init() {
  printf("[track] Started!\n");
  fflush(stdout);
}

__attribute__((destructor))
static void testrun_my_deinit() {
  printf("[track] Stopped!\n");
  fflush(stdout);
}

static int callback(struct dl_phdr_info *info, size_t size, void *data) {
  printf("[track] dl_iterate_phdr reports `%s' at %zx\n", info->dlpi_name, info->dlpi_addr);
  return 0;
}

void testrun_init() {
  printf("[track] Early initialization!\n");
  dl_iterate_phdr(callback, NULL);
  fflush(stdout);
}

void testrun_opened(size_t from, size_t to, const char* path, uintptr_t* ud) {
  printf("[track] Range [%zx,%zx) mapped to `%s'\n", from, to, path);
  fflush(stdout);
  *ud = (uintptr_t)strdup(path);
}

void testrun_closed(uintptr_t* ud) {
  char* path = (char*)*ud;
  printf("[track] Unmapped `%s'\n", path);
  fflush(stdout);
  free(path);
}

void testrun_found(uintptr_t* in, const char* name, size_t start, size_t end) {
  const char* path = *in == UD_MISSED ? "(missed)" : *in == UD_TRACKER ? "(tracker)" : (const char*)*in;
  printf("[track] Found symbol `%s` from `%s' at [%zu,%zu)\n", name, path, start, end);
  fflush(stdout);
}

void testrun_bound(uintptr_t* from, uintptr_t* to, const char* name, size_t start, size_t end) {
  const char* fpath = *from == UD_MISSED ? "(missed)" : *from == UD_TRACKER ? "(tracker)" : (const char*)*from;
  const char* tpath = *to == UD_MISSED ? "(missed)" : *to == UD_TRACKER ? "(tracker)" : (const char*)*to;
  printf("[track] `%s' bound symbol `%s' from `%s' at [%zx,%zx)\n", fpath, name, tpath, start, end);
  fflush(stdout);
}
