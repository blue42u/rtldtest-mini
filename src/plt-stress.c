#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

#define N 100

extern void inner();

int main() {
  struct timespec start;
  struct timespec end;

  inner();

  clock_gettime(CLOCK_REALTIME, &start);
  for(int i = 0; i < N; i++) inner();
  clock_gettime(CLOCK_REALTIME, &end);

  double t_start = (double)start.tv_sec*1000000000. + (double)start.tv_nsec;
  double t_end = (double)end.tv_sec*1000000000. + (double)end.tv_nsec;
  printf("ns per call: %f\n", (t_end - t_start) / N);
  return 0;
}
