#!/bin/bash

HERE="`dirname "$0"`"

TRACKER="`realpath "$HERE"/libtest.so`"
AUDITOR="`realpath "$HERE"/libtestaudit-eager.so`"

LD_PRELOAD="$TRACKER":"$LD_PRELOAD" LD_AUDIT="$AUDITOR":"$LD_AUDIT" TESTRUN_LIBRARY="$TRACKER" \
exec "$@"
